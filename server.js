const express = require("express");
const path = require("path");
const app = express();

app.set('view engine', 'ejs');

const createPath = (page) => path.resolve(__dirname, 'views', `${page}.ejs`)

const port = process.env.PORT || 3000;
app.listen(port, (err)=>{
    err?console.log(err):console.log(`listening on port ${port}`);
});

app.get('/', (req,res)=>{
    res.render(createPath('index'));
})